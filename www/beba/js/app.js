var api_v1 = "http://127.0.0.1:8000/api/v1";

// var api_v1 = "http://bebaapp.tk/api/v1";

var appTitle = "Beba App";

$(document).ready(function () {

	$(".appTitle").html(appTitle);

});

//Getting  User Level 
function getUserLevel() {
 return localStorage.getItem("userlevel");
}

//Setting  User Level
function setUserLevel(userlevel) {
  localStorage.setItem("userlevel", userlevel); 
}

//Getting  Driver Special Id 
function getSpecialDriverId() {
 return localStorage.getItem("specialdriverid");
}

//Setting  Driver Special Id
function setSpecialDriverId(specialdriverid) {
  localStorage.setItem("specialdriverid", specialdriverid); 
}

//Getting  Driver Name 
function getDriverName() {
 return localStorage.getItem("drivername");
}

//Setting  Driver Name
function setDriverName(drivername) {
  localStorage.setItem("drivername", drivername); 
}

//Getting  Driver Info Id
function getDriverInfoId() {
 return localStorage.getItem("driverinfoid");
}

//Setting  Driver Info Id
function setDriverInfoId(driverinfoid) {
  localStorage.setItem("driverinfoid", driverinfoid); 
}

//Getting Specific Driver Id
function getSpecificDriverId() {
 return localStorage.getItem("driverSpecificId");
}

//Setting Specific Driver Id
function setSpecificDriverId(driverSpecificId) {
  localStorage.setItem("driverSpecificId", driverSpecificId); 
}

//Getting Driver Id
function getDriverId() {
 return localStorage.getItem("driverId");
}

//Setting Driver Id
function setDriverId(driverId) {
  localStorage.setItem("driverId", driverId); 
}

//Getting TransportCat
function getTransportCat() {
 return localStorage.getItem("transport");
}

//Setting TransportCat
function setTransportCat(transport) {
  localStorage.setItem("transport", transport); 
}

//Getting Location 2 (Cargo To)
function getLocation2() {
 return localStorage.getItem("location2");
}

//Setting Location 2
function setLocation2(location2) {
  localStorage.setItem("location2", location2); 
}


//Getting Location 1 (Cargo From)
function getLocation1() {
 return localStorage.getItem("location1");
}

//Setting Location 1
function setLocation1(location1) {
  localStorage.setItem("location1", location1); 
}

//Redirect to Dashboard
function redirectToDashboard() {
  window.location.assign('dashboard.html');
}

//Getting Username
function getUsername() {
 return localStorage.getItem("username");
}

//Setting Username
function setUsername(username) {
	localStorage.setItem("username", username);	
}

function removeUser()
{
	localStorage.setItem("username", "");		
}


window.fn = {};

window.fn.open = function() {
  var menu = document.getElementById('menu');
  menu.open();
};

window.fn.load = function(page) {
  var content = document.getElementById('content');
  var menu = document.getElementById('menu');
  content.load(page)
    .then(menu.close.bind(menu));
};


function appLogout()
{
	removeUser();
  localStorage.clear();

	window.location.assign("../index-2.html");
}


